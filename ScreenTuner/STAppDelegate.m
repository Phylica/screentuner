//
//  STAppDelegate.m
//  ScreenTuner
//
//  Created by Pierre Marandon on 13/06/2014.
//  Created by PHPdev32 on 2/19/13.
//  Licensed under GPLv3, full text at http://www.gnu.org/licenses/gpl-3.0.txt
//

#import "STAppDelegate.h"
#import "DDC.h"
#import <IOKit/hidsystem/ev_keymap.h>
#import "STAppDelegate.h"

@implementation FSApplication

-(void)sendEvent:(NSEvent *)theEvent{
    [super sendEvent:theEvent];
    if (theEvent.type == NSSystemDefined
        && theEvent.subtype == NX_SUBTYPE_AUX_CONTROL_BUTTONS
        && theEvent.data2 == NSUIntegerMax
        && (theEvent.data1 >> 8 & 0xFF) == NX_KEYDOWN
        && [self.delegate respondsToSelector:@selector(mediaKeyDown:)])
        [self.delegate performSelector:@selector(mediaKeyDown:) withObject:@(theEvent.data1 >> 16 & 0xFF)];
}

@end

@implementation DDC {
@private
    NSUInteger _control;
    dispatch_semaphore_t _block;
    CGDirectDisplayID _display;
    NSNumber *_value;
}

@synthesize max = _max;

+(NSString *)EDIDString:(char *)string{
    NSString *temp = [[NSString alloc] initWithBytes:string length:13 encoding:NSASCIIStringEncoding];
    return ([temp rangeOfString:@"\n"].location != NSNotFound)
    ? [[temp componentsSeparatedByString:@"\n"] objectAtIndex:0]
    : temp;
}

-(id)initWithControl:(NSUInteger)control on:(CGDirectDisplayID)display {
    self = [super init];
    if (self) {
        _enabled = true;
        _block = dispatch_semaphore_create(1);
        _display = display;
        _control = control;
    }
    return self;
}
-(NSUInteger)max {
    if (!_value)
        [self value];
    return _max;
}
-(NSUInteger)value{
    if (!_value) {
        dispatch_semaphore_wait(_block, DISPATCH_TIME_FOREVER);
        struct DDCReadCommand command = {_control};
        if (!DDCRead(_display, &command)) {
            muteWithNotice(self, enabled, _enabled = false);
            _value = @0;
        }
        else
            _value = @(command.current_value);
        dispatch_semaphore_signal(_block);
        assignWithNotice(self, max, command.max_value);
        delayWithNotice(self, value, 0);
    }
	return _value.unsignedLongValue;
}
-(void)setValue:(NSUInteger)value{
    if (!_enabled)
        return;
    dispatch_semaphore_wait(_block, DISPATCH_TIME_FOREVER);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        struct DDCWriteCommand command = {_control, value};
        if (!DDCWrite(_display, &command)) {
            muteWithNotice(self, enabled, _enabled = false);
        }
        dispatch_semaphore_signal(_block);
    });
    _value = nil;
}

@end

@implementation STAppDelegate{

@private
    NSPointerArray *_displayIDs;
}

#pragma mark NSApplicationDelegate
-(void)awakeFromNib {
    _displayIDs = [NSPointerArray pointerArrayWithOptions:NSPointerFunctionsOpaqueMemory | NSPointerFunctionsIntegerPersonality];
    [self applicationDidChangeScreenParameters:nil];
    [self addObserver:self forKeyPath:@"displayNumber" options:NSKeyValueObservingOptionInitial context:0];

    NSMenu *mainMenu = [[NSMenu allocWithZone:[NSMenu menuZone]] initWithTitle:@"ST"];
  	// this menu item will have a view with one NSButton
    NSMenuItem *mainItem = [[NSMenuItem allocWithZone:[NSMenu menuZone]] initWithTitle:@"[main item]" action:NULL keyEquivalent:@""];
    [mainItem setEnabled:YES];
    //	[mainItem setView:mainMenuView];
    //	[mainItem setTarget:self];
    [mainMenu addItem:mainItem];
	[mainItem setView:mainMenuView];
	[mainItem setTarget:self];

[mainMenu addItem:[NSMenuItem separatorItem]];

    NSMenuItem *minimumItem = [[NSMenuItem allocWithZone:[NSMenu menuZone]] initWithTitle:@"Minimum" action:NULL keyEquivalent:@""];
    [minimumItem setEnabled:YES];
    [minimumItem setTarget:self];
    [minimumItem setAction:@selector(makeMinimum:)];
	[mainMenu addItem:minimumItem];

    NSMenuItem *maximumItem = [[NSMenuItem allocWithZone:[NSMenu menuZone]] initWithTitle:@"Maximum" action:NULL keyEquivalent:@""];
    [maximumItem setEnabled:YES];
    [maximumItem setTarget:self];
    [maximumItem setAction:@selector(makeMaximum:)];
	[mainMenu addItem:maximumItem];

	[mainMenu addItem:[NSMenuItem separatorItem]];


    NSMenuItem *quitItem = [[NSMenuItem allocWithZone:[NSMenu menuZone]] initWithTitle:@"Close" action:NULL keyEquivalent:@""];
    [quitItem setEnabled:YES];
    [quitItem setTarget:NSApp];
    [quitItem setAction:@selector(terminate:)];
	[mainMenu addItem:quitItem];

    statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSSquareStatusItemLength] ;
    [statusItem setImage:[NSImage imageNamed:@"STIcon"]];
    [statusItem setMenu:mainMenu];
    [statusItem setTitle:@""];
    [statusItem setHighlightMode:YES];
}


-(void)applicationDidChangeScreenParameters:(NSNotification *)notification {
    CGDirectDisplayID old = _displayNumber && _displayNumber <= _displayIDs.count
    ? (CGDirectDisplayID)[_displayIDs pointerAtIndex:_displayNumber - 1]
    : 0;
    while (_displayIDs.count)
        [_displayIDs removePointerAtIndex:0];
    for (NSScreen *screen in NSScreen.screens) {
        CGDirectDisplayID new = [[screen.deviceDescription objectForKey:@"NSScreenNumber"] unsignedIntValue];
        [_displayIDs addPointer:(void *)(UInt64)new];
        if (new == old && _displayNumber != _displayIDs.count)
            self.displayNumber = _displayIDs.count;
    }
    _displaysView.numberOfTickMarks = NSScreen.screens.count;
    assignWithNotice(self, allDisplays, _displaysView.numberOfTickMarks);
    if (!old)
        self.displayNumber = 1;
}

#pragma mark Actions
-(IBAction)refresh:(id)sender {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self observeValueForKeyPath:nil ofObject:nil change:nil context:NULL];
    });
}

#pragma mark Media Keys
- (void)mediaKeyDown:(NSNumber *)mediaKey {
    switch (mediaKey.intValue) {
        case NX_KEYTYPE_MUTE:
            if (_mute)
                _mute.value = !_mute.value;
            break;
        case NX_KEYTYPE_SOUND_UP:
            if (_volume)
                _volume.value = MIN(_volume.max, _volume.value + _volume.max / 16);
            [[NSSound soundNamed:@"volume"] play];
            break;
        case NX_KEYTYPE_SOUND_DOWN:
            if (_volume)
                _volume.value = MAX(0, _volume.value - _volume.max / 16);
            [[NSSound soundNamed:@"volume"] play];
            break;
    }
}

#pragma mark Observation
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    CGDirectDisplayID display = (CGDirectDisplayID)[_displayIDs pointerAtIndex:_displayNumber - 1];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        struct EDID edid = {};
        if (EDIDTest(display, &edid))
            dispatch_async(dispatch_get_main_queue(), ^{
                for (NSValue *value in @[[NSValue valueWithPointer:&edid.descriptor1], [NSValue valueWithPointer:&edid.descriptor2], [NSValue valueWithPointer:&edid.descriptor3], [NSValue valueWithPointer:&edid.descriptor4]]) {
                    union descriptor *des = value.pointerValue;
                    switch (des->text.type) {
                        case 0xFF:
                            assignWithNotice(self, serial, [DDC EDIDString:des->text.data]);
                            break;
                        case 0xFC:
                            assignWithNotice(self, name, [DDC EDIDString:des->text.data]);
                            break;
                    }
                }
                self.brightness = [[DDC alloc] initWithControl:BRIGHTNESS on:display];
                self.contrast = [[DDC alloc] initWithControl:CONTRAST on:display];
                self.volume = [[DDC alloc] initWithControl:AUDIO_SPEAKER_VOLUME on:display];
                self.mute = [[DDC alloc] initWithControl:AUDIO_MUTE on:display];
                self.reset = [[DDC alloc] initWithControl:RESET on:display];
            });
        else
            dispatch_async(dispatch_get_main_queue(), ^{
                assignWithNotice(self, serial, nil);
                assignWithNotice(self, name, nil);
                self.brightness = self.contrast = self.volume = self.mute = nil;
                NSRunCriticalAlertPanel(@"EDID Not Found", @"Could not fetch the EDID, controls will be unavailable", nil, nil, nil);
            });
    });
}

-(IBAction)makeMinimum:(id)sender {
    self.brightness.value=0;
    self.contrast.value=0;
}

-(IBAction)makeMaximum:(id)sender {
    self.brightness.value=self.brightness.max;
    self.contrast.value=self.contrast.max;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}



@end



