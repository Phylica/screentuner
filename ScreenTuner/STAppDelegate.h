//
//  STAppDelegate.h
//  ScreenTuner
//
//  Created by Pierre Marandon on 13/06/2014.
//  Created by PHPdev32 on 2/19/13.
//  Licensed under GPLv3, full text at http://www.gnu.org/licenses/gpl-3.0.txt
//

#import <Cocoa/Cocoa.h>

@interface FSApplication : NSApplication

@end

@interface DDC : NSObject

@property (readonly) NSUInteger max;
@property (readonly) bool enabled;
@property NSUInteger value;

-(id)initWithControl:(NSUInteger)control on:(CGDirectDisplayID)display;

@end

@interface STAppDelegate : NSObject <NSApplicationDelegate>{
    NSStatusItem * statusItem;
    IBOutlet NSView *mainMenuView;
}

@property (assign) IBOutlet NSSlider *displaysView;
@property (readonly) NSString *serial;
@property (readonly) NSString *name;
@property NSUInteger displayNumber;
@property (readonly) NSUInteger allDisplays;
@property DDC *brightness;
@property DDC *contrast;
@property DDC *volume;
@property DDC *mute;
@property DDC *reset;

@end
