//
//  main.m
//  ScreenTuner
//
//  Created by Pierre Marandon on 13/06/2014.
//  Licensed under GPLv3, full text at http://www.gnu.org/licenses/gpl-3.0.txt
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    NSSound *volume = [[NSSound alloc] initWithContentsOfFile:@"/System/Library/LoginPlugins/BezelServices.loginPlugin/Contents/Resources/volume.aiff" byReference:true];
    volume.name = @"volume";
    return NSApplicationMain(argc, argv);
}
